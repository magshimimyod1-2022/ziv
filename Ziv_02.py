import socket
import datetime


def check_weather(city: str, date: str):
    IP = "34.218.16.79"
    PORT = 77


    request_format = f"100:REQUEST:city={city}&date={date}&checksum={get_checksum(city, date)}"

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((IP, PORT))

        # Get the opening message
        sock.recv(1024)

        # Ask for weather
        sock.sendall(request_format.encode())

        response = sock.recv(1024).decode()

    # return should be like (16.5, “Few Clouds”)
    if "ERROR" not in response:
        weather = (float(response.split("&")[2].replace("temp=", "")), response.split("&")[3].replace("text=", ""))
    else:
        weather = (999, response.replace("500:ERROR:", ""))
    return weather

def get_checksum(city: str, date: str):
    DIFFERENCE = 96
    city_cal = 0
    date_cal = 0

    # Calculate city (part 1) - alphabet a = 1, b = 2, c = 3 etc
    for letter in city:
        ascii = ord(letter.lower())
        city_cal += ascii - DIFFERENCE

    for digit in date.replace(r"/", ""):
        date_cal += int(digit)

    # Calculate date (part 2) - sum of digits
    return str(city_cal) + "." + str(date_cal)

def main():
    city_input = input("Enter your city by it's full name: ")
    option = int(input("Choose your option:\n1 - Today's weather\n2 - Today's and next 3 days weather\n>>> "))

    current_day = datetime.date.today()
    # Format of dd/mm/yy
    current_day = datetime.date.strftime(current_day, "%d/%m/20%y")

    if option == 1: # Today
        print(check_weather(city_input, current_day))
    else: # Next 3 days
        dates = [current_day]
        for i in range(3):
            # Insert 3 next days dates to the list
            date = datetime.date.today() + datetime.timedelta(days= i + 1)
            dates += [datetime.date.strftime(date, "%d/%m/20%y")]

        for day_date in dates:
            result = check_weather(city_input, day_date)
            print(day_date + ", Temperature: " + str(result[0]) + "," + result[1] + ".")

if __name__ == '__main__':
    main()
