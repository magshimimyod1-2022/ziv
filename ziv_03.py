import socket

PORT = 9090
PORT_REQUEST = 92
IP = "54.71.128.194"
data_from_server = ""
data_to_server = ""
blacklist_found = False
blacklist_content = ""

print("Starting...")

try:
    with open("blacklist.txt", "r") as file:
        blacklist_content = file.read()
        file.close()

    blacklist_found = True
except FileNotFoundError or FileExistsError as e:
    print("Blacklist not found: ", e)

def main():
    global blacklist_found
    while True:
        banned = False
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server:
            server.bind(("", PORT))

            print("Listening...")

            server.listen(1)

            client, address = server.accept()

            # 1. Recv the request from the client. And ask later as the client
            data_to_server = client.recv(1024).decode()
            print("Recv from client: " + data_to_server)

            country = data_to_server.lower().split("country:")[1]
            # France is banned
            if "france" in country:
                client.send('ERROR#"France is banned!"'.encode())
                continue

            # Not using `with` because the previous connection will be closed
            server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

            server.connect((IP, PORT_REQUEST))

            # Ask as the client the data
            server.send(data_to_server.encode())

            # Get the url to the image or the data
            data_from_server = server.recv(1024).decode()
            print("From real server: " + data_from_server)

            # Add . to the url
            data_from_server = data_from_server.replace("jpg", ".jpg")
            print("After replacement: " + data_from_server)

            # Black list bonus
            if blacklist_found and "error" not in data_from_server.lower():
                for name in blacklist_content.split("\n"):
                    if name in data_from_server.split("actors:")[1] and name:
                        client.send(f'ERROR#"Film is banned because of {name}"'.encode())
                        print(name + " Is banned!!!!!!!")
                        banned = True;
                        break
            if banned:
                continue

            # 4. Return the correct url to the client
            client.send(data_from_server.encode())

if __name__ == '__main__':
    main()
